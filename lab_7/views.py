from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse,HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()

    friend_list = Friend.objects.all()
    response = {"mahasiswa_list": mahasiswa_list, "friend_list": friend_list}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']
        if Friend.objects.filter(npm = npm):#Dapetin di object friend di database dengan npm tertentu & ngecek apakah list kosong atau tidak
        	return HttpResponseBadRequest()
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = model_to_dict(friend)
        return HttpResponse(data)#harus dictionary

def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/')

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    is_taken = False

    for mahasiswa in mahasiswa_list:
    	if npm == mahasiswa["npm"]:
    		is_taken = True
    		break
    data = {
        'is_taken': is_taken #lakukan pengecekan apakah Friend dgn npm tsb sudah ada
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]["fields"])
    return data
    
def friend_list_json(request):
	friend_list = Friend.objects.all()
	friend_lst =[]
	for friend in friend_list:
		friend_dict = {
			'name':friend.friend_name,
			'npm':friend.npm
		}
		friend_lst.append(friend_dict)
	return JsonResponse(friend_lst, safe=False	)